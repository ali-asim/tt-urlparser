# TT - URL Parser

This is frontend experiment of an url parser.

# Setup

```sh   
  - update your host file to   
   127.0.0.1       pf.tradetracker.net
  - git glone, or extract zip
  $ npm install
  $ npm run dev
  - browser will open up pf.tradetracker.net:8080 (you may update this under build/dev-server.js)
  - use the following url (which is also the default feed url) to test functionality
    http://pf.tradetracker.net:8080/static/productfeed.xml
```

### Technology
- Node js
- Vue js
- Javascript


### Modules
- Vue Cli - project scalfolding
- Webpack - builder / task runner
- Axios - for get requests
- Fast XML Parser for converting xml to json
- vee Validator for validation
